# The Earth’s dynamo model

**PD leader:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/CNRS.png?inline=false" width="80">

**Partners:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/IPGP.png?inline=false" width="80">

**Codes:** 

**Derived Simulation Cases:** [SC7.1](https://gitlab.com/cheese5311126/simulation-cases/sc7.1)

## Description

The Earth's magnetic field is generated and maintained by turbulent flow of liquid metal in the Earth's
outer core. Its variability exhibits a wide range of time and length scales, from interannual pulses of secular acceleration to
the occasional reversal of the geomagnetic field. This system is characterized by a high degree of scale separation: the
magnetic field is large-scale while flow and buoyancy (which is of thermo-chemical origin) are small-scale, on the account of
their smaller diffusivity coefficients. The developments in ChEESE-1P were constrained to a limited time span and an
arguably limited range of spatial scales, which may be detrimental to the quality of short-term geomagnetic forecasts. This
PD will extend the previous developments by embracing the full spectrum of flow motion at stake and enabling a longer time
integration by using a handful of very high-resolution simulations (over short time spans) to train dedicated neural networks
for subgrid-scale modeling. The work plan consists of 3 successive steps: 1) use AI to infer the free parameters of standard
subgrid-scale models using Neural Networks (NN). This should work well for hydrodynamic simulations (no magnetic field),
and also for realistic dynamo simulations with scale-separation (a feature of the real Earth core) if the subgrid-scale model
operates below the magnetic dissipation scale. 2) use AI to infer the subgrid-scale contribution, in a fully data-driven way. No
explicit models are assumed. This will require more data to train the model, but should allow more accurate subgrid-scale
models. The NN will take advantage of physical invariants and symmetries, and 3) the final step will be to implement
end-to-end training that requires a fully differentiable code.

## Objective

The main objectives of this PD are: (1) simulate the geodynamo process with an unprecedented level of realism,
most notably by acknowledging that the fluid flow is highly turbulent and small-scale, while the magnetic field is large-scale,
(2) assess the impact of turbulent flow on the short and long term working of the geodynamo, and propose an AI-based
parameterization of subgrid-scale fluxes that is physics-informed and, (3) include the effect of double-diffusive convection on
the geodynamo model, through its impact on the subgrid-scale fluxes and on the overall dynamics.